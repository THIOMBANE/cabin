import java.lang.Math.PI
import java.lang.Math.sqrt

open class RoundHut(val residents:Int, val radius:Double) : Dwelling(residents) {
               override val buildingMaterial = "straw"
               override val capacity = 4

    override fun floorArea(): Double {
        return PI * radius * radius
    }

    fun calculateMaxCarpetSize():Double {
        val diameter = 2 * radius
        return sqrt (diameter * diameter / 2)
    }

}